local curl_http = require('curl_http')

local cat_type = {
  { '演员#', 'star' }, { '演員#', 'star' }, { '歌手#', 'star' }, { '音樂團體', 'star' },
  { '年出生', 'person' }, { '年逝世', 'person' },
  { '城市#', 'place' },  { '国家#', 'place' },  { '國家#', 'place' }, { '政權#', 'place' }, { '共和国#', 'place' }, { '成員國#', 'place' }, 
  { '节日#', 'festival' }, { '節日#', 'festival' }, { '主题日#', 'festival' },
  { '事件#', 'event' }, { '案件#', 'event' }, { '事故#', 'event' }, { '罪案#', 'event' },
  { '网站#', 'website' }, { '網站#', 'website' },
}

local cat_topic = {
  { '電視劇', 'drama' }, { '电视剧', 'drama' }, { '漫畫', 'acg' }, { '漫画', 'acg' }, 
  { '動畫', 'acg' }, { '动画', 'acg' }, { '遊戲#', 'acg' }, { '游戏#', 'acg' }, { '角色列表', 'acg' },
  { '劇集', 'drama' }, { '剧集', 'drama' }, { '電影#', 'film' }, { '电影#', 'film' },
  { '色情', 'porn' }, { '性行为', 'porn' }, { '%f[%a]AV%f[%A]', 'porn' }, 
  { '黨', 'politics' }, { '党', 'politics' }, { '中共', 'politics' }, { '政治', 'politics' }, { '選舉', 'politics' }, { '选举', 'politics' },
  { '军事', 'military' }, { '軍事', 'military' }, { '战争#', 'military' }, { '戰爭#', 'military' }, { '战役#', 'military' }, { '戰役#', 'military' },
  { '真人秀', 'show' }, { '綜藝', 'show' }, { '综艺', 'show' }, { '选秀', 'show' }, { '娛樂節目', 'show' }, { '娱乐节目', 'show' }, 
  { '演唱會', 'show' }, { '演唱会', 'show' }, { '音乐比赛', 'show' },
  { '冠狀病毒病', 'covid19' }, { '冠状病毒病', 'covid19' },
  { '病', 'medicine' }, { '药', 'medicine' }, { '疫苗', 'medicine' },
}

local exclusion = { '党员', '校友', '维基数据' }
local function in_exclusion(title)
  for _, v in ipairs(exclusion) do
    if title:match(v) then return true end
  end
  return false
end

local f = io.open(os.getenv("HOME") .. '/replica.my.cnf')
local conf = f:read('*all')
local username = conf:match('\nuser = (%S+)')
local password = conf:match('\npassword = (%S+)')

local mysql = require('mysql')
mysql.bind('mariadb')
local host = 'zhwiki.analytics.db.svc.eqiad.wmflabs'
local conn = mysql.connect(host, username, password, 'zhwiki_p', 'utf8mb4')

local function get_cats(article)
  local sql_str = 'SELECT cl_to FROM categorylinks WHERE cl_from = (' ..
    'SELECT page_id FROM page WHERE page_title = "' .. conn:escape(article) ..
    '" AND page_namespace = 0 AND page_is_redirect = 0 )'
  conn:query(sql_str)
  local result = conn:store_result()
  local cats = {}
  local cats_index = 0
  for _, cat in result:rows() do
    cats_index = cats_index + 1
    cats[cats_index] = cat
  end
  return cats
end

function get_tags(categories)
  categories = table.concat(table.map(categories, function(x)
    if in_exclusion(x) then return '' end
    return x
  end), '#') .. '#'
  local art_type, topic = 'others', 'others'
  for _, v in ipairs(cat_type) do
    local result = categories:match(v[1])
    if result then
      art_type = v[2]
      break
    end
  end
  for _, v in ipairs(cat_topic) do
    local result = categories:match(v[1])
    if result then
      topic = v[2]
      break
    end
  end
  return art_type, topic
end

return {
  get_cats = get_cats,
  get_tags = get_tags
}
