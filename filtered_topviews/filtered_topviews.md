# Filtered topviews

As Wikimedia Analytics topviews data doesn't filter out some false positives, I wrote this service to provide yesterday's topviews for Chinese Wikipedia, with article types and topics automatically marked.

The URL currently serving is https://alex-wiki.toolforge.org/topviews

## Parameters

| Name  | Description                                                  |
| ----- | ------------------------------------------------------------ |
| type  | the nature or type of articles<br>Available values: star, person, place, festival, event, website, others |
| topic | the topic of the articles<br>Available values: drama, acg, film, porn, politics, military, show, covid19, medicine |
| nrows | numbers of articles to return<br>Default: 10                 |

For `type` and `topic` , the values can be combined with `|` . For example, get top articles which is a drama or a film or ACG related

```
GET https://alex-wiki.toolforge.org/topviews?topic=drama|acg|film
```

Also, you can get top articles excluding specific types, with  `!` flag. For example, get top articles which is a star but not a porn star

```
GET https://alex-wiki.toolforge.org/topviews?type=star&topic=!porn
```

## Comment

Data is only stored for one day, and will be replaced at 10 am (UTC+8) the next day.