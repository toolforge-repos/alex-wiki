local curl_http = require('curl_http')

local namespaces = {
  Wikipedia = true, Special = true, Category = true, Portal = true,
  Template = true, File = true
}

local json = require('cjson')
local top_url = 'https://wikimedia.org/api/rest_v1/metrics/pageviews/top/zh.wikipedia.org'
local top_date = os.date('%Y/%m/%d', os.time() - 86400)
local all_access = {}
local all_access_dict = {}
local res, err, ecode = curl_http.httpsget(top_url .. '/all-access/' .. top_date, 3)
local count = 0
if res then
  local result = json.decode(res)

  for _, v in ipairs(result.items[1].articles) do
    local ns = v.article:match('^(.-):')
    if ns then ns = ns:gsub('_talk', '') end
    if not namespaces[ns] then
      count = count + 1
      all_access[count] = v
      all_access_dict[v.article] = count
    end
  end
else
  error(err)
end

res, err, ecode = curl_http.httpsget(top_url .. '/desktop/' .. top_date, 3)
if res then
  local result = json.decode(res)

  for i, v in ipairs(result.items[1].articles) do
    local article_obj = all_access[all_access_dict[v.article]]
    if article_obj then
      article_obj.positive = true
      local div = v.views / article_obj.views
      if div > 0.95 or div < 0.05 then
        article_obj.positive = false
      end
    end
  end
else
  error(err)
end

local topart = {}
count = 0
for i, v in ipairs(all_access) do
  if v.positive then
    count = count + 1
    topart[count] = v
  end
end

local f = io.open(os.getenv("HOME") .. '/replica.my.cnf')
local conf = f:read('*all')
local username = conf:match('\nuser = (%S+)')
local password = conf:match('\npassword = (%S+)')

local mysql = require('mysql')
mysql.bind('mariadb')
local host = 'tools.db.svc.eqiad.wmflabs'
local conn = mysql.connect(host, username, password, username .. '__wmc', 'utf8mb4')

local categorize = require('filtered_topviews/categorize')

table.map = function(t, mapFunc)
  local out = {}
  for i, v in ipairs(t) do
    out[i] = mapFunc(v, i, t)
  end
  return out
end

conn:query('TRUNCATE TABLE zhtop')
local sql_str = 'INSERT INTO zhtop (article, type, topic, views) VALUES ' .. 
  table.concat(table.map(topart, function(item)
    local cats = categorize.get_cats(item.article)
    local art_type, topic = categorize.get_tags(cats)
    return string.format('("%s","%s","%s","%s")', item.article, art_type, topic, item.views)
  end), ',')
conn:query(sql_str)

conn:close()
print('Succssfully stored topviews for ' .. top_date)
